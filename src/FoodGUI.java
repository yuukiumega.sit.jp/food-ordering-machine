import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane textPane1;
    private JButton sushiButton;
    private JButton yakitoriButton;
    private JButton sobaButton;
    private JButton checkoutButton;
    private JTextPane textPane2;

    int total = 0;

    void order(String food,int price){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " +food+" ?",
                "order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if (confirmation == 0) {
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you for ordering " + food + "! it will be served as soon as possible.");
            total = total + price;
            String currentText = textPane1.getText();
            textPane1.setText(currentText + food +"   " + price + "yen\n");
            textPane2.setText(" " + total+"yen");
        }
    }
    public FoodGUI() {
        textPane2.setText(" " + total+"yen");
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",600);
            }
        });
        ImageIcon image0 = new ImageIcon("src/image/天ぷら.jpg");
        tempuraButton.setIcon(image0);

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",520);
            }
        });
        ImageIcon image1 = new ImageIcon("src/image/ラーメン.jpg");
        ramenButton.setIcon(image1);

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Udon",360);
            }
        });
        ImageIcon image2 = new ImageIcon("src/image/うどん.jpg");
        udonButton.setIcon(image2);

        sushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Sushi",980);
            }
        });
        ImageIcon image3 = new ImageIcon("src/image/すし.jpg");
        sushiButton.setIcon(image3);

        yakitoriButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Yakitori",330);
            }
        });
        ImageIcon image4 = new ImageIcon("src/image/焼き鳥.jpg");
        yakitoriButton.setIcon(image4);

        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Soba",480);
            }
        });
        ImageIcon image5 = new ImageIcon("src/image/そば.jpg");
        sobaButton.setIcon(image5);

        checkoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(
                            null,
                            "Thank you! The total price is " + total + "yen");
                    total = 0;
                    textPane2.setText(" " + total+"yen");
                    textPane1.setText("");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
